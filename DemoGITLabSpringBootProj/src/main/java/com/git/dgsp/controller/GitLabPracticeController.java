package com.git.dgsp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gitlab")
public class GitLabPracticeController {

	/**
	 * @apiNote this api is created for greeting user 
	 * @return String
	 *  
	 * @author Kamlesh Dev-1
	 */
	public String getGreetingMsg() {
		return "Welcome to the GitLab";
	}
}
