package com.git.dgsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGitLabSpringBootProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGitLabSpringBootProjApplication.class, args);
	}

}
